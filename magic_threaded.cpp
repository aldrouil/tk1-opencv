#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/gpu/gpu.hpp>
#include <pthread.h>
#include <fstream>
#include <sstream>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <mutex>


using namespace cv;
using namespace std;
using namespace gpu;

#define PORT 1337 //cant use ports lower than 1024 (e.g. 772), reserved by system
#define IP "127.0.0.1" //IP to send to
#define STREAMADDR "http://FRC:FRC@192.168.1.135/mjpg/video.mjpg"
//#define STREAMADDR "http://64.251.85.30/mjpg/video.mjpg"

Mat inputimage;
Mat src;
Mat dst;
Mat src1,src2;
Mat mask;
//Mat test;

GpuMat gpu_src, gpu_inputimage, gpu_src1, gpu_src2, gpu_mid1, gpu_mid2;

Point centroid;
Point centroid1;

int H,S,V,HU,SU,VU;

string videoStreamAddress = STREAMADDR;

vector<vector<Point> > contours1;
vector<vector<Point> > contours2;
vector<Vec4i> hierarchy;

Scalar white = Scalar( 255, 255, 255 );
Scalar green = Scalar( 18, 185, 0 );

int largest_area = 0;
int largest_contour_index = 0;
int second_largest_index = 0;
int second_largest_contour = 0;

std::mutex grabmtx;
std::mutex gpumtx;
std::mutex cpumtx1;
std::mutex cpumtx2;

void *GrabImage(void *threadid){
	//this process grabs the image and puts it in the buffer
	long tid;
	tid = (long)threadid;
	//VideoCapture vcap(videoStreamAddress);
	VideoCapture vcap(0);

	for(;;)
	{
		vcap.grab();

		//lock the memory space when updating
		grabmtx.lock();
		vcap >> inputimage;
		//inputimage = imread("IMG_6773.jpg");
		gpu_inputimage.upload(inputimage);

		//unlock memory space
		grabmtx.unlock();
	}
}

void *UDPSend(void *threadid){
	//This thread send the UDP packets to the python script.
	long tid;
	tid = (long)threadid;

	int sock = socket( AF_INET,SOCK_DGRAM,0);
    //error if socket not created
    if(sock<=0){
        cout << "Error. Failed to create network socket. Aborting.\n";
        return false;
    }

    //create socket destination IP and Port
    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr(IP);
    address.sin_port = htons(PORT);

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)

    if ( bind(sock,(const sockaddr*) &address,sizeof(sockaddr_in) ) < 0 ){
        cout << "Error. Failed to bind socket to port. Aborting.\n";
        return false;
    }


    for(;;)
    {
	    double centerx_1 = centroid.x;
	    double centery_1 = centroid.y;
	    double centerx_2 = centroid1.x;
	    double centery_2 = centroid1.x;
	    double distance = 9000;
	    //convert packet data to a string
	    ostringstream mid_string;
	    mid_string << centerx_1 << " "<< centery_1 << " " << centerx_2 << " " << centery_2 << " " << distance;
	    string packet_string = mid_string.str();
	    //cout << "string thing " << packet_string << endl << endl;


	    //convert packet string to a char and get its size
	    char *packet_char = &packet_string[0];
	    int packet_size=packet_string.length();
	    //cout << packet_char << " what im sending " << endl << endl;
	    //cout << packet_size << " size of what im sending " << endl << endl;
	    //send the packet
	    //int sent_bytes = sendto(sock,packet_char,packet_size,0,(sockaddr*)&address,sizeof(sockaddr_in) );
	    int sent_bytes = sendto(sock,packet_char,packet_size,0,(sockaddr*)&address,sizeof(sockaddr_in) );

	    //Error if packet size doesnt match data size
	    if ( sent_bytes != packet_size ){
	        cout << "Error. Failed to send packet.\n";
	        return false;
	    }

	    usleep(500000);
    }
}

void *imgCPUthread1(void *threadid){
	//this process finds the contours
	for(;;){

		//inRange(src,Scalar(H,S,V),Scalar(HU,SU,VU),mask);
		//src1 = mask.clone();

		//cast off values hight and lower than Scalar(H,S,V) and Scalar(HU,SU,VU)
		//first split vals into H,S,V channels
		//GpuMat gpu_split[3];
		//GpuMat gpu_split_dst1[3];
		//GpuMat gpu_split_dst2[3];
		vector< gpu::GpuMat> gpu_split(3);
vector< gpu::GpuMat> gpu_split_dst1(3);
vector< gpu::GpuMat> gpu_split_dst2(3);

gpu::cvtColor(gpu_inputimage,gpu_src,CV_RGB2HSV);

		GpuMat gpumerge;
		gpu::split(gpu_src,gpu_split);
		//cast off min values
		gpu::threshold(gpu_split[0],gpu_split_dst1[0],H,0,THRESH_TOZERO); //0 is only used if thresh type is binary
		gpu::threshold(gpu_split[1],gpu_split_dst1[1],S,0,THRESH_TOZERO);
		gpu::threshold(gpu_split[2],gpu_split_dst1[2],V,0,THRESH_TOZERO);
		//cast off max values
		gpu::threshold(gpu_split_dst1[0],gpu_split_dst2[0],HU,0,THRESH_TOZERO_INV);
		gpu::threshold(gpu_split_dst1[1],gpu_split_dst2[1],SU,0,THRESH_TOZERO_INV);
		gpu::threshold(gpu_split_dst1[2],gpu_split_dst2[2],VU,0,THRESH_TOZERO_INV);
		//merge images
		//gpu::merge(gpu_split_dst2,3,gpumerge);
		gpu::merge(gpu_split_dst2,gpumerge);
		gpumerge.download(src1);

		cpumtx1.lock();
		findContours(src1,contours1,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_TC89_KCOS, Point(0,0));
		gpu_src1.upload(src1);
		cpumtx1.unlock();
	}
}

void *imgCPUthread2(void *threadid){
	//this process draws the bounding rectangles and circles
	for(;;){


		vector<Rect> boundRect(contours1.size());

		for (int i = 0 ; i < contours1.size(); i++)
		{
			boundRect[i] = boundingRect(Mat(contours1[i]));
		}

		for (int i = 0 ; i < contours1.size(); i++)
		{
			double a = boundRect[i].area();
			if ( a > largest_area)
			{
				second_largest_contour = largest_area;
				second_largest_index = largest_contour_index;
				largest_area=a;
				largest_contour_index = i;
			}
		}

		//cout << "Largest Area " << largest_area << endl;
		//cout << "Second Largest Area " << second_largest_contour << endl;

		drawContours(src2, contours2, largest_contour_index, white, CV_FILLED, 8, hierarchy);
		drawContours(src2, contours2, second_largest_index, white, CV_FILLED, 8, hierarchy);

		if (largest_area > 1000)
		{
			//cout << largest_area << endl;
			//rectangle( src2, boundRect[largest_contour_index].tl(), boundRect[largest_contour_index].br(), green, 2, 8, 0 );
			Point centroid;

			cpumtx2.lock();
			centroid.x= boundRect[largest_contour_index].x + (boundRect[largest_contour_index].width / 2);
			centroid.y =boundRect[largest_contour_index].y + (boundRect[largest_contour_index].height / 2);
			cpumtx2.unlock();
			//cout << centroid.x << "," << centroid.y << endl;
			//circle(src2,centroid,5, Scalar(255,0,255), 1);
		}
		if (second_largest_contour > 1000)
		{
			//cout << second_largest_contour << endl;
			//rectangle( src2, boundRect[second_largest_index].tl(), boundRect[second_largest_index].br(), green, 2, 8, 0 );
			Point centroid1;

			cpumtx2.lock();
			centroid1.x= boundRect[second_largest_index].x + (boundRect[second_largest_index].width / 2);
			centroid1.y =boundRect[second_largest_index].y + (boundRect[second_largest_index].height / 2);
			cpumtx2.unlock();

			//cout << centroid.x << "," << centroid.y << endl;
			//circle(src2,centroid1,5, Scalar(255,0,255), 1);
		}
	}
}


void *imgGPUprocess1(void *threadid){
	for (;;){
		//gpu_inputimage.upload(inputimage);
/*
		gpumtx.lock();
		gpu::cvtColor(gpu_inputimage,gpu_src,CV_RGB2HSV);
		//gpumtx.lock();
		gpu_src.download(src);
		gpumtx.unlock();
*/
		//gpu_src1.upload(src1);
		gpu::cvtColor(gpu_src1,gpu_src2,CV_GRAY2RGB);
		gpumtx.lock();
		gpu_src2.download(src2);
		gpumtx.unlock();

	}

}

int main() {
	VideoCapture vcap(0);
	//VideoCapture vcap(videoStreamAddress);

	cout << "Opening video device...........";
	if(!vcap.isOpened()){
		cout << "Error.\nNo video device found.\n";
		return 0;
	}
	vcap.grab();
	vcap >> inputimage;
	vcap.release();
	cout <<"Done.\n";


	//check if GPU available
	cout << "Initialising GPU.............";

	//initiallise GPU
	gpu::setDevice(0);


	/*if(gpu::getCudaEnabledDeviceCount()){
		cout << "Error.\nNo GPU Found.\n";
		return -1;
	}
*/
	cout << "Done.\n";

	ifstream in;
    in.open("t_values.txt");
		cout << "Opening t_values.txt........";
    in >> H >> S >> V >> HU >> SU >> VU;
    in.close();
		cout << "Done\n";

	//populate images matricies at beginning with the correct data types by running through code once
	//avoids a segmentation fault
	cout << "Populating matricies with correct initial data.........";
	gpu_inputimage.upload(inputimage);
	gpu::cvtColor(gpu_inputimage,gpu_src,CV_BGR2HSV);
	gpu_src.download(src);
	inRange(src,Scalar(H,S,V),Scalar(HU,SU,VU),mask);
	src1 = mask.clone();
	findContours(src1,contours1,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_TC89_KCOS, Point(0,0));
	gpu_src1.upload(src1);
	gpu::cvtColor(gpu_src1,gpu_src2,CV_GRAY2RGB);
	gpu_src2.download(src2);
	cout << "Done.\n";



	int i;
	pthread_t threads[5];
	cout << "main() : creating thread, " << 0 << " GrabImage" << endl;
	pthread_create(&threads[0], NULL, GrabImage, (void *)0);
	cout << "main() : creating thread, " << 1 << " UDPSend" << endl;
	pthread_create(&threads[1], NULL, UDPSend, (void *)1);
	cout << "main() : creating thread, " << 2 << " imgCPUthread1" << endl;
	pthread_create(&threads[2], NULL, imgCPUthread1, (void *)2);
	cout << "main() : creating thread, " << 3 << " imgCPUthread2" << endl;
	pthread_create(&threads[3], NULL, imgCPUthread2, (void *)3);
	cout << "main() : creating thread, " << 4 << " imgGPUprocess1" << endl;
	pthread_create(&threads[4], NULL, imgGPUprocess1, (void *)4);

	//pthread_join pauses threads until they complete
	pthread_join(threads[0], NULL);
	pthread_join(threads[1], NULL);
	pthread_join(threads[2], NULL);
	pthread_join(threads[3], NULL);
	pthread_join(threads[4], NULL);
	return 0;
}
