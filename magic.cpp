#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/gpu/gpu.hpp>
#include <pthread.h>
#include <fstream>
#include <sstream>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>


using namespace cv;
using namespace std;
using namespace gpu;

#define PORT 1337 //cant use ports lower than 1024 (e.g. 772), reserved by system
#define IP "127.0.0.1" //IP to send to

CudaMem page_locked(Size(640,480), CV_32FC3);
Mat inputimage = page_locked;
Mat src;
Mat dst;
Mat src1;
Mat mask;
Mat test;

Point centroid;
Point centroid1;

int H,S,V,HU,SU,VU;

void *GrabImage(void *threadid)
{
	long tid;
	tid = (long)threadid;
	string videoStreamAddress = "http://FRC:FRC@192.168.1.135/mjpg/video.mjpg";
	VideoCapture vcap(videoStreamAddress);
	
	for(;;)
	{
		vcap.grab();
		vcap >> inputimage;
	}
}

void *UDPSend(void *threadid)
{
	long tid;
	tid = (long)threadid;

	int sock = socket( AF_INET,SOCK_DGRAM,0);
    //error if socket not created
    if(sock<=0){
        cout << "Error. Failed to create network socket. Aborting.\n";
        return false;
    }

    //create socket destination IP and Port
    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr(IP);
    address.sin_port = htons(PORT);

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)

    if ( bind(sock,(const sockaddr*) &address,sizeof(sockaddr_in) ) < 0 ){
        cout << "Error. Failed to bind socket to port. Aborting.\n";
        return false;
    }


    for(;;)
    {
	    double centerx_1 = centroid.x;
	    double centery_1 = centroid.y;
	    double centerx_2 = centroid1.x;
	    double centery_2 = centroid1.x;
	    double distance = 9000;
	    //convert packet data to a string
	    ostringstream mid_string;
	    mid_string << centerx_1 << " "<< centery_1 << " " << centerx_2 << " " << centery_2 << " " << distance;
	    string packet_string = mid_string.str();
	    //cout << "string thing " << packet_string << endl << endl;


	    //convert packet string to a char and get its size
	    char *packet_char = &packet_string[0];
	    int packet_size=packet_string.length();
	    //cout << packet_char << " what im sending " << endl << endl;
	    //cout << packet_size << " size of what im sending " << endl << endl;
	    //send the packet
	    //int sent_bytes = sendto(sock,packet_char,packet_size,0,(sockaddr*)&address,sizeof(sockaddr_in) );
	    int sent_bytes = sendto(sock,packet_char,packet_size,0,(sockaddr*)&address,sizeof(sockaddr_in) );

	    //Error if packet size doesnt match data size
	    if ( sent_bytes != packet_size ){
	        cout << "Error. Failed to send packet.\n";
	        return false;
	    }

	    usleep(500000);
    }
}

int main() {

	cv::gpu::setDevice(0);
	ifstream in;
    in.open("t_values.txt");
    in >> H >> S >> V >> HU >> SU >> VU;
    in.close();
	
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	int i;
	pthread_t threads[2];
	cout << "main() : creating thread, " << 0 << endl;
	pthread_create(&threads[0], NULL, GrabImage, (void *)0);
	cout << "main() : creating thread, " << 1 << endl;
	pthread_create(&threads[1], NULL, UDPSend, (void *)1);

	for(;;) 
	{
		
		double t = (double)getTickCount();
		cvtColor(inputimage,src,CV_RGB2HSV);
		inRange(src,Scalar(H,S,V),Scalar(HU,SU,VU),mask);

		src1 = mask.clone();

		findContours(src1,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_TC89_KCOS, Point(0,0));
	
		cvtColor(src1,src1,CV_GRAY2RGB);
		
		int largest_area = 0;
		int largest_contour_index = 0;
		int second_largest_index = 0;
		int second_largest_contour = 0;
		vector<Rect> boundRect(contours.size());
		for (int i = 0 ; i < contours.size(); i++)
		{
			boundRect[i] = boundingRect( Mat(contours[i]));
		}

		for (int i = 0 ; i < contours.size(); i++)
		{
			double a = boundRect[i].area();
			if ( a > largest_area)
			{
				second_largest_contour = largest_area;
				second_largest_index = largest_contour_index;
				largest_area=a;
				largest_contour_index = i;
			}
		}

		//cout << "Largest Area " << largest_area << endl;
		//cout << "Second Largest Area " << second_largest_contour << endl;

		Scalar white = Scalar( 255, 255, 255 );
		Scalar green = Scalar( 18, 185, 0 );
		drawContours ( src1, contours, largest_contour_index, white, CV_FILLED, 8, hierarchy);
		drawContours ( src1, contours, second_largest_index, white, CV_FILLED, 8, hierarchy);

		if (largest_area > 1000)
		{
			//cout << largest_area << endl;			
			rectangle( src1, boundRect[largest_contour_index].tl(), boundRect[largest_contour_index].br(), green, 2, 8, 0 );
			Point centroid;
			centroid.x= boundRect[largest_contour_index].x + (boundRect[largest_contour_index].width / 2);
			centroid.y =boundRect[largest_contour_index].y + (boundRect[largest_contour_index].height / 2);
			//cout << centroid.x << "," << centroid.y << endl;
			circle(src1,centroid,5, Scalar(255,0,255), 1);
		}
		if (second_largest_contour > 1000)
		{
			//cout << second_largest_contour << endl;
			rectangle( src1, boundRect[second_largest_index].tl(), boundRect[second_largest_index].br(), green, 2, 8, 0 );
			Point centroid1;
			centroid1.x= boundRect[second_largest_index].x + (boundRect[second_largest_index].width / 2);
			centroid1.y =boundRect[second_largest_index].y + (boundRect[second_largest_index].height / 2);
			//cout << centroid.x << "," << centroid.y << endl;
			circle(src1,centroid1,5, Scalar(255,0,255), 1);
		}

		
		t = ((double)getTickCount() - t)/getTickFrequency();
		imshow( "Original", inputimage);
 		imshow( "Threshold", mask );
 		imshow( "Targets", src1);

		if(waitKey(25) >= 0)
		{
			break;
		}
	}
return 0;


}
