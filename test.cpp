#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/gpu/gpu.hpp>
#include <pthread.h>
#include <fstream>
#include <sstream>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>


using namespace cv;
using namespace std;
using namespace gpu;

int main() {
GpuMat gpu_imgbuffer;
  Mat imgbuffer;
cv::VideoCapture vcap(0);

for(;;){
  vcap.grab();
vcap >> imgbuffer;

imshow( "Original", imgbuffer);
cvWaitKey(10);
}
}
