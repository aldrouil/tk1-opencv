#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fstream>
#include <sstream>
#include <time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <thread>
#include <mutex>
#include <exception>

#include <QtNetwork>
#include <QtCore>
#include <QtGui>

using namespace cv;
using namespace std;

#define PORT 1337 //cant use ports lower than 1024 (e.g. 772), reserved by system
#define IP "127.0.0.1" //IP to send to

//#define BOUNDARY 'Content-type','multipart/x-mixed-replace; boundary=--jpgboundary'
#define BOUNDARY --boundary

Mat imgbuffer;
mutex imglock;
cv::VideoCapture vcap(0);

QImage mat2qimg(Mat opencv_img){

        Mat dest;
        cv::cvtColor(opencv_img, dest, CV_BGR2RGB);
        QImage qimg((uchar*)dest.data, dest.cols, dest.rows,QImage::Format_RGB888);
        return qimg;
}

void udp_thread(){

        quint16 udp_port = PORT;
        QHostAddress addr = QHostAddress(IP);
        QUdpSocket sock;
        sock.bind(addr,udp_port,QAbstractSocket::ShareAddress);


/*
        if(!sock.isOpen()) {
                cout << "Error. Failed to open socket. Aborting.........\n";
                throw;
        }
 */

        sock.waitForReadyRead(-1);
        QString inbound = sock.readAll();

        QByteArray content_type_header;
        content_type_header = ("HTTP/1.0 200 OK\r\n" \
                               "Server: tk1.perigrine.ca example server\r\n" \
                               "Cache-Control: no-cache\r\n" \
                               "Cache-Control: private\r\n" \
                               "Content-Type: multipart/x-mixed-replace;boundary=--boundary\r\n\r\n");

        sock.write(content_type_header);

        //initialize imgbuffer
        vcap.grab();
        imglock.lock();
        vcap >> imgbuffer;
        imglock.unlock();

        for(;; ) {
                //encode img to string content buffer
                vector<unsigned char> buff;
                imencode(".jpg",imgbuffer,buff);
                string content(buff.begin(), buff.end());

                QByteArray CurrentImg(QByteArray::fromStdString(content));

                QByteArray BoundaryString = ("--boundary\r\n" \
                                             "Content-Type: image/jpeg\r\n" \
                                             "Content-Length: ");
                BoundaryString.append(QString::number(CurrentImg.length()));
                BoundaryString.append("\r\n\r\n");
                sock.write(BoundaryString);
                sock.write(CurrentImg); //write the encoded image
                sock.flush();
        }


}

int main() {

        //cv::VideoCapture vcap(0);

        thread thread1(udp_thread);

        for(;; ) {
                vcap.grab();

                imglock.lock();
                vcap >> imgbuffer;

                imglock.unlock();

                imshow( "Original", imgbuffer);
                cvWaitKey(10);
        }
        thread1.join();
}
