cmake_minimum_required(VERSION 3.1 FATAL_ERROR)
project( opencv_streaming.o )
add_executable(streamer.o main.cpp)
set(CMAKE_AUTOMOC ON)

set_property(TARGET streamer.o PROPERTY CXX_STANDARD 11)
set_property(TARGET streamer.o PROPERTY CXX_STANDARD_REQUIRED ON)

find_package(OpenCV REQUIRED)

find_package(PythonLibs)

find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Network REQUIRED)
include_directories(${Qt5Core_INCLUDES} ${Qt5Network_INCLUDES} ${Qt5Gui_INCLUDES})

target_link_libraries(streamer.o ${OpenCV_LIBS} ${Qt5Core_LIBRARIES} ${Qt5Network_LIBRARIES} ${Qt5Gui_LIBRARIES})
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Core_EXECUTABLE_COMPILE_FLAGS} ${Qt5Network_EXECUTABLE_COMPILE_FLAGS} ${Qt5Gui_EXECUTABLE_COMPILE_FLAGS} -std=c++11")
