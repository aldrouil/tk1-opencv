FRC Team 772 TK1 code repository.

magic.cpp is the working code deployed for the 2016 Windsor regional to the Nvidia Jetson TK1.

magic_threaded.cpp is a threaded version of the same code that currently segfaults. 

OpenCV is required to build this project. This project requireds OpenCV to be built with CUDA support. 

Project is meant to be run on a computer running linux. Confirmed working on Arch Linux x86_64 (April 17th, 2016) and on the Nvidia Jetson TK1 as of the time of the competitions.